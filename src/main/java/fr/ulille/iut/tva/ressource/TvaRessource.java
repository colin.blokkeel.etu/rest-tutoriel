package fr.ulille.iut.tva.ressource;

import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;

/**
 * TvaRessource
 */
@Path("tva")
public class TvaRessource {
    private CalculTva calculTva = new CalculTva();
    
    @GET
    @Path("tauxpardefaut")
    public double getValeurTauxParDefaut() {
    	return TauxTva.NORMAL.taux;
    }
    
    @GET
    @Path("valeur/{niveauxTva}")
    public double getValeurTaux(@PathParam("niveauxTva") String niveau) {
    	return TauxTva.valueOf(niveau.toUpperCase()).taux;
    }
    
    @GET
    @Path("{niveauxTva}")
    public String getMontantTotal(@PathParam("niveauxTva") String niveau, @QueryParam("somme") int somme) {
    	return "taux de tva : " + getValeurTaux(niveau) + "\nsomme : " + somme + "\n";
    }
}
